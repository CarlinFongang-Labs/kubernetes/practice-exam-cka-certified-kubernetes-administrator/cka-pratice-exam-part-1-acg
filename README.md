# 1. Lab : Scénarios de Pratique pour l'Examen CKA - ACG

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs 

1. Comptez le nombre de nœuds prêts à exécuter des charges de travail normales

2. Récupérer les messages d'erreur d'un journal de conteneur

3. Recherchez le pod portant l'étiquette app=auth dans l'espace de noms Web qui utilise le plus de processeur.

# Contexte : 


Cette question utilise le cluster `acgk8s`. Après vous être connecté au serveur d'examen, passez au contexte correct avec la commande `kubectl config use-context acgk8s`.

Chacun des objectifs représente une tâche que vous devrez accomplir en utilisant le cluster et le(s) serveur(s) disponibles. Lisez attentivement chaque objectif et réalisez la tâche spécifiée.

Pour certains objectifs, vous devrez peut-être vous connecter via SSH à d'autres nœuds ou serveurs à partir du serveur d'examen. Vous pouvez le faire en utilisant le nom d'hôte/nom de nœud (par exemple, `ssh acgk8s-worker1`).

Remarque : Vous ne pouvez pas vous connecter via SSH à un autre nœud, ni utiliser kubectl pour vous connecter au cluster, depuis un nœud autre que le nœud racine. Une fois que vous avez terminé les tâches nécessaires sur un serveur, assurez-vous de quitter et de revenir au nœud racine avant de continuer.

Si vous devez assumer les privilèges root sur un serveur, vous pouvez le faire avec `sudo -i`.

Vous pouvez exécuter le script de vérification situé à `/home/cloud_user/verify.sh` à tout moment pour vérifier votre travail !

>![Alt text](img/image.png)


# Introduction
Ce laboratoire fournit des scénarios de pratique pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Vous serez présenté avec des tâches à accomplir ainsi que des serveurs et/ou un cluster Kubernetes existant pour les compléter. Vous devrez utiliser vos connaissances de Kubernetes pour réussir les tâches fournies, tout comme vous le feriez lors de l'examen CKA réel. Bonne chance !

# Application

## Étape 1 : Connexion au Serveur

Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Compter le nombre de nœuds prêts à exécuter des charges de travail normales

1. Passez au contexte approprié avec kubectl :

```sh
kubectl config use-context acgk8s
```

>![Alt text](img/image-1.png)

2. Comptez le nombre de nœuds prêts à exécuter une charge de travail normale :

```sh
kubectl get nodes
```

>![Alt text](img/image-2.png)
*2 noeud disponible sur le cluster*

3. Vérifiez que les nœuds worker peuvent exécuter des charges de travail normales :

```sh
kubectl describe node acgk8s-worker1
```

   - Faites défiler vers le haut de la sortie et vérifiez la liste des **"taints"**. Vous ne devriez en voir aucun.

>![Alt text](img/image-3.png)
*Aucun taint disponible*

4. Répétez les étapes ci-dessus pour `acgk8s-worker2`. Vous ne devriez voir aucun taint sur ce nœud non plus.

>![Alt text](img/image-4.png)
*Aucun taint disponible*

5. Enregistrez ce nombre dans le fichier `/k8s/0001/count.txt` :

```sh
echo 2 > /k8s/0001/count.txt
```

6. Exécutez le script de vérification pour vérifier votre travail :

```sh
./verify.sh
```

Contenu du script
```bash
#!/bin/bash
echo -e "Checking Objectives..."
OBJECTIVE_NUM=0
function printresult {
  ((OBJECTIVE_NUM+=1))
  echo -e "\n----- Checking Objective $OBJECTIVE_NUM -----"
  echo -e "----- $1"
  if [ $2 -eq 0 ]; then
      echo -e "      \033[0;32m[COMPLETE]\033[0m Congrats! This objective is complete!"
  else
      echo -e "      \033[0;31m[INCOMPLETE]\033[0m This objective is not yet completed!"
  fi
}

expected='2'
actual=$(cat /k8s/0001/count.txt 2>/dev/null)
[[ "$actual" = "$expected" ]]
printresult "Count the number of nodes that are ready to run normal workloads." $?

expected='[ERROR] Could not process record 006c27
[ERROR] Could not process record 01a45c'
actual=$(cat /k8s/0002/errors.txt 2>/dev/null)
[[ "$actual" = "$expected" ]]
printresult "Retrieve error messages from a container log." $?

expected='auth-web'
actual=$(cat /k8s/0003/cpu-pod.txt 2>/dev/null)
[[ "$actual" = "$expected" ]]
printresult "Find the Pod that is utilizing the most CPU within a Namespace." $?
```

## Étape 3 : Récupérer les messages d'erreur d'un journal de conteneur

1. Obtenez les messages d'erreur du journal d'un conteneur :

```sh
kubectl logs -n backend data-handler -c proc
```

>![Alt text](img/image-5.png)
*Lignes d'erreurs identifiées*

2. Retournez uniquement les messages d'erreur :

```sh
kubectl logs -n backend data-handler -c proc | grep ERROR
```

>![Alt text](img/image-6.png)

3. Enregistrez cette sortie dans le fichier `/k8s/0002/errors.txt` :

```sh
kubectl logs -n backend data-handler -c proc | grep ERROR > /k8s/0002/errors.txt
```

>![Alt text](img/image-7.png)
*Enregistrement des lignes d'erreurs*

4. Exécutez le script de vérification pour vérifier votre travail :

```sh
./verify.sh
```

>![Alt text](img/image-8.png)


## Étape 4 : Trouver le pod avec un label `app=auth` dans le namespace `web` qui utilise le plus de CPU

1. Avant de faire cette étape, veuillez attendre une minute ou deux pour donner à notre script backend le temps de générer une charge CPU. Localisez quel pod dans le namespace `web` avec l'étiquette `app=auth` utilise le plus de CPU (dans certains cas, d'autres pods peuvent apparaître comme consommant plus de CPU) :

```sh
kubectl top pod -n web --sort-by cpu --selector app=auth
```

>![Alt text](img/image-9.png)
*Tri des pod par odre décroissant de consommation CPU*

2. Enregistrez le nom de ce pod dans `/k8s/0003/cpu-pod.txt` :

```sh
echo auth-web > /k8s/0003/cpu-pod.txt
```

>![Alt text](img/image-10.png)
*Sauvegarde des informations sur le pod avec la plus haute consommation*

3. Exécutez le script de vérification pour vérifier votre travail :

```sh
./verify.sh
```

>![Alt text](img/image-11.png)

Ce laboratoire vous guide à travers des scénarios pratiques pour vous préparer à l'examen CKA, y compris la gestion des nœuds, la récupération des journaux de conteneurs et l'analyse de l'utilisation du CPU des pods.